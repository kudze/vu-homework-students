package kk.studentai.window;

import javafx.stage.Stage;

public abstract class AbstractWindow {
    private final Stage stage;
    private WindowCloseListener closeListener;

    public AbstractWindow(Stage stage) {
        this.stage = stage;
    }

    public void show() {
        this.getStage().show();
    }

    public void close() {
        this.getStage().close();

        if(closeListener != null)
            closeListener.onWindowClose();
    }

    public WindowCloseListener getCloseListener() {
        return closeListener;
    }

    public void setCloseListener(WindowCloseListener closeListener) {
        this.closeListener = closeListener;
    }

    public Stage getStage() {
        return stage;
    }
}
