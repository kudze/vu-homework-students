package kk.studentai.window;

import javafx.scene.Scene;
import javafx.stage.Stage;

public class PopupWindow extends AbstractWindow {
    public PopupWindow(Scene scene, String title)
    {
        super(new Stage());

        this.getStage().setTitle(title);
        this.getStage().setResizable(false);
        this.getStage().setScene(scene);
        this.getStage().sizeToScene();

        this.show();
    }

}
