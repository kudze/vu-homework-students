package kk.studentai.window;

public interface WindowCloseListener {
    void onWindowClose();
}
