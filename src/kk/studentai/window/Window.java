package kk.studentai.window;

import javafx.stage.Stage;
import javafx.scene.Scene;

public class Window extends AbstractWindow {
    public Window(int width, int height, String title, boolean resizeable)
    {
        this(new Stage(), width, height, title, resizeable);
    }

    public Window(Stage stage, int width, int height, String title, boolean resizeable)
    {
        super(stage);
        this.getStage().setWidth(width);
        this.getStage().setHeight(height);
        this.getStage().setTitle(title);
    }

    public void setScene(Scene scene)
    {
        this.getStage().setScene(scene);
    }
}
