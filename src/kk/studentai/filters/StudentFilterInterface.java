package kk.studentai.filters;

import kk.studentai.models.Student;

import java.util.List;

public interface StudentFilterInterface {
    List<Student> filterStudents(List<Student> students);
}
