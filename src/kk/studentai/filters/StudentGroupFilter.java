package kk.studentai.filters;

import kk.studentai.models.Student;
import kk.studentai.models.StudentGroup;

import java.util.ArrayList;
import java.util.List;

public class StudentGroupFilter implements StudentFilterInterface {
    private final StudentGroup group;

    public StudentGroupFilter(StudentGroup group) {
        this.group = group;
    }

    @Override
    public List<Student> filterStudents(List<Student> students) {
        ArrayList<Student> result = new ArrayList<>();

        for (Student student : students)
            if (this.group.getStudents().contains(student))
                result.add(student);

        return result;
    }

    public StudentGroup getStudentGroup() {
        return group;
    }
}
