package kk.studentai.filters;

import kk.studentai.models.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentFilter implements StudentFilterInterface {
    private final Student student;

    public StudentFilter(Student student) {
        this.student = student;
    }

    @Override
    public List<Student> filterStudents(List<Student> students) {
        ArrayList<Student> result = new ArrayList<>();

        for (Student student : students)
            if (this.student == student)
                result.add(student);

        return result;
    }

    public Student getStudent() {
        return student;
    }
}
