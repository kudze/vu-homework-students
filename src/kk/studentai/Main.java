package kk.studentai;

import javafx.application.Application;
import javafx.stage.Stage;
import kk.studentai.models.managers.StudentManager;
import kk.studentai.tasks.Task;
import kk.studentai.view.HomeView;
import kk.studentai.window.PopupWindow;
import kk.studentai.window.Window;

public class Main extends Application {
    private static Main instance;

    public static Main getInstance() {
        return Main.instance;
    }

    private Window window;
    private PopupWindow popupWindow;
    private StudentManager studentManager = new StudentManager();
    private HomeView homeView;

    @Override
    public void start(Stage stage) throws Exception {
        Main.instance = this;
        this.studentManager.load();

        this.homeView = new HomeView();

        this.window = new Window(
                stage,
                1800,
                720,
                "Studentų registracijos sistema | Karolis Kraujelis",
                false
        );

        this.window.setScene(homeView.buildScene());
        this.window.show();
    }

    @Override
    public void stop() throws Exception {
        this.studentManager.save();
    }

    public void launchTask(Task task)
    {
        this.stopTask();

        popupWindow = task.launchTask();

        if(popupWindow != null)
            popupWindow.setCloseListener(() -> popupWindow = null);
    }

    public void stopTask()
    {
        homeView.update();

        if(popupWindow == null)
            return;

        popupWindow.close();
        popupWindow = null;
    }

    public Window getWindow() {
        return window;
    }

    public PopupWindow getPopupWindow() {
        return popupWindow;
    }

    public boolean isPopupActive() {
        return popupWindow != null;
    }

    public StudentManager getStudentManager() { return studentManager; }

    public static void main(String[] args) {
        launch(args);
    }
}
