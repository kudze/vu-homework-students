package kk.studentai.tasks;

import kk.studentai.view.ExportStudentsView;

public class ExportStudentsTask extends Task {
    public ExportStudentsTask()
    {
        super(new ExportStudentsView(), "Eksportuoti studentus");
    }
}
