package kk.studentai.tasks;

import kk.studentai.view.CreateStudentGroupView;
import kk.studentai.view.CreateStudentView;

public class CreateStudentGroupTask extends Task {

    public CreateStudentGroupTask() {
        super(new CreateStudentGroupView(), "Sukurti studentų grupę");
    }

}
