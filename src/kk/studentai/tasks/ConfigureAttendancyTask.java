package kk.studentai.tasks;

import kk.studentai.view.ConfigureAttendancyView;

public class ConfigureAttendancyTask extends Task {

    public ConfigureAttendancyTask() {
        super(new ConfigureAttendancyView(), "Žymėti studentų lankomumą");
    }

}
