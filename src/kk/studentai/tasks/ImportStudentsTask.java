package kk.studentai.tasks;

import com.opencsv.*;
import com.opencsv.exceptions.CsvException;
import javafx.stage.FileChooser;
import kk.studentai.Main;
import kk.studentai.models.Student;
import kk.studentai.models.StudentGroup;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ImportStudentsTask extends Task {
    private static final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");

    public ImportStudentsTask() {
        super(null, "Importuoti studentus");

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Pasirinkite failą");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("CSV failas", "*.csv"),
                new FileChooser.ExtensionFilter("Excel failas", "*.xlsx")
        );
        File file = fileChooser.showOpenDialog(null);

        if (file == null || !file.getName().contains("."))
            return;

        String extension = file.getName().split("\\.", 2)[1];

        if (extension.equalsIgnoreCase("csv"))
            this.importCSV(file);

        else if (extension.equalsIgnoreCase("xlsx"))
            this.importXLSX(file);
    }

    private void importCSV(File file) {
        try {
            CSVParser csvParser = new CSVParserBuilder()
                    .withSeparator(';')
                    .withQuoteChar(CSVWriter.DEFAULT_QUOTE_CHARACTER)
                    .build();

            Reader reader = Files.newBufferedReader(file.toPath());
            CSVReader csvReader = new CSVReaderBuilder(reader).withCSVParser(csvParser).build();

            List<String[]> rows = csvReader.readAll();
            ArrayList<Date> dates = new ArrayList<>();

            boolean first = true;
            for (String[] row : rows) {
                if (first) {
                    for (int i = 3; i < row.length; i++) {
                        dates.add(dateFormatter.parse(row[i]));
                    }

                    first = false;
                } else {
                    String firstName = row[0];
                    String lastName = row[1];
                    String groups = row[2];

                    TreeMap<Date, Boolean> attendance = new TreeMap<>();
                    for (int i = 3; i < row.length; i++) {
                        String value = row[i];

                        if (value.equalsIgnoreCase("Dalyvavo"))
                            attendance.put(dates.get(i - 3), true);

                        else if (value.equalsIgnoreCase("Nedalyvavo"))
                            attendance.put(dates.get(i - 3), false);
                    }

                    Student student = new Student(
                            firstName,
                            lastName,
                            attendance
                    );
                    Main.getInstance().getStudentManager().addStudent(student);

                    String[] groupNames = groups.split("\\|");
                    for (String title : groupNames) {
                        StudentGroup group = Main.getInstance().getStudentManager().getStudentGroupByTitle(title);

                        if (group == null) {
                            group = new StudentGroup(title);
                            Main.getInstance().getStudentManager().addStudentGroup(group);
                        }

                        group.addStudent(student);
                    }
                }
            }

            csvReader.close();
            reader.close();
        } catch (IOException | CsvException | ParseException | IndexOutOfBoundsException exception) {

        }
    }

    private void importXLSX(File file) {
        try {
            FileInputStream is = new FileInputStream(file);
            XSSFWorkbook workbook = new XSSFWorkbook(is);
            XSSFSheet sheet = workbook.getSheetAt(0);

            ArrayList<Date> dates = new ArrayList<>();

            int i = 0;
            for (Row row : sheet) {
                String firstName = null, lastName = null, groups = null;
                TreeMap<Date, Boolean> attendance = new TreeMap<>();

                int j = 0;
                for (Cell cell : row) {

                    if (i == 0) {
                        if (j >= 3) dates.add(dateFormatter.parse(cell.getStringCellValue()));
                    } else {
                        if (j == 0) firstName = cell.getStringCellValue();
                        else if (j == 1) lastName = cell.getStringCellValue();
                        else if (j == 2) groups = cell.getStringCellValue();
                        else {
                            if (cell.getStringCellValue().equalsIgnoreCase("Dalyvavo"))
                                attendance.put(dates.get(j - 3), true);
                            else if (cell.getStringCellValue().equalsIgnoreCase("Nedalyvavo"))
                                attendance.put(dates.get(j - 3), false);
                        }
                    }

                    j++;
                }

                if(i != 0) {
                    Student student = new Student(
                            firstName,
                            lastName,
                            attendance
                    );
                    Main.getInstance().getStudentManager().addStudent(student);

                    String[] groupNames = groups.split("\\|");
                    for (String title : groupNames) {
                        StudentGroup group = Main.getInstance().getStudentManager().getStudentGroupByTitle(title);

                        if (group == null) {
                            group = new StudentGroup(title);
                            Main.getInstance().getStudentManager().addStudentGroup(group);
                        }

                        group.addStudent(student);
                    }
                }

                i++;
            }

            workbook.close();
            is.close();
        } catch (IOException | ParseException exception) {

        }
    }
}
