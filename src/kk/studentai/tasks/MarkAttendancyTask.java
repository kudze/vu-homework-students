package kk.studentai.tasks;

import kk.studentai.filters.StudentFilterInterface;
import kk.studentai.view.ConfigureAttendancyStudentView;
import kk.studentai.view.MarkAttendancyView;

import java.util.Date;

public class MarkAttendancyTask extends Task {

    public MarkAttendancyTask(StudentFilterInterface filter, Date date) {
        super(new MarkAttendancyView(filter, date), "Žymėti studentų lankomumą");
    }

}
