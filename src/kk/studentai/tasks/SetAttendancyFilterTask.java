package kk.studentai.tasks;

import kk.studentai.view.AbstractView;
import kk.studentai.view.SetAttendancyFilterView;
import kk.studentai.view.components.StudentAttendancyFilter;

public class SetAttendancyFilterTask extends Task {

    public SetAttendancyFilterTask(StudentAttendancyFilter filter) {
        super(new SetAttendancyFilterView(filter), "Filtro nustatymai");
    }

}
