package kk.studentai.tasks;

import javafx.scene.Scene;
import kk.studentai.view.AbstractView;
import kk.studentai.window.PopupWindow;

public class Task {
    private AbstractView view;
    private String title;

    public Task(AbstractView view, String title) {
        this.view = view;
        this.title = title;
    }

    public PopupWindow launchTask() {
        if(this.view == null)
            return null;

        return new PopupWindow(this.getScene(), this.getTitle());
    }

    public AbstractView getView() {
        return view;
    }

    public Scene getScene() {
        if(this.view == null)
            return null;

        return view.buildScene();
    }

    public String getTitle() {
        return title;
    }
}
