package kk.studentai.tasks;

import kk.studentai.view.ConfigureAttendancyStudentView;

public class ConfigureAttendancyStudentTask extends Task {

    public ConfigureAttendancyStudentTask() {
        super(new ConfigureAttendancyStudentView(), "Žymėti studentų lankomumą");
    }

}
