package kk.studentai.tasks;

import kk.studentai.view.CreateStudentView;

public class CreateStudentTask extends Task {

    public CreateStudentTask() {
        super(new CreateStudentView(), "Pridėti studentą");
    }

}
