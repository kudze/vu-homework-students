package kk.studentai.tasks;

import kk.studentai.models.StudentGroup;
import kk.studentai.view.EditStudentGroupView;

public class EditStudentGroupTask extends Task {
    public EditStudentGroupTask(StudentGroup group) {
        super(new EditStudentGroupView(group), "Redaguoti grupės informaciją");
    }
}
