package kk.studentai.tasks;

import kk.studentai.view.AddStudentToGroupView;

public class AddStudentToGroupTask extends Task {

    public AddStudentToGroupTask() {
        super(new AddStudentToGroupView(), "Pridėti studentą prie grupės");
    }

}
