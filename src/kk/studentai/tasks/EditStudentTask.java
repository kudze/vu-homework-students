package kk.studentai.tasks;

import kk.studentai.models.Student;
import kk.studentai.view.EditStudentView;

public class EditStudentTask extends Task {
    public EditStudentTask(Student student) {
        super(new EditStudentView(student), "Redaguoti studento informaciją");
    }
}
