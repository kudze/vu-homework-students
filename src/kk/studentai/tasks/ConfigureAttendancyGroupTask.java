package kk.studentai.tasks;

import kk.studentai.view.ConfigureAttendancyGroupView;

public class ConfigureAttendancyGroupTask extends Task {

    public ConfigureAttendancyGroupTask() {
        super(new ConfigureAttendancyGroupView(), "Žymėti studentų lankomumą");
    }

}
