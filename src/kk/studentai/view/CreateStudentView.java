package kk.studentai.view;

import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import kk.studentai.Main;
import kk.studentai.models.Student;
import kk.studentai.view.components.CHBox;
import kk.studentai.view.components.RedText;

public class CreateStudentView extends AbstractView {
    private TextField firstNameField = new TextField();
    private TextField lastNameField = new TextField();
    private RedText firstNameError = new RedText();
    private RedText lastNameError = new RedText();

    @Override
    protected Parent init() {
        Button createBtn = new Button("Išsaugoti");
        Button cancelBtn = new Button("Atšaukti");

        createBtn.setOnAction(evt -> this.save());
        cancelBtn.setOnAction(evt -> this.cancel());

        VBox result = new VBox(
                25,
                new VBox(
                        new Label("Vardas:"),
                        firstNameField,
                        firstNameError
                ),
                new VBox(
                        new Label("Pavardė:"),
                        lastNameField,
                        lastNameError
                ),
                new CHBox(
                        25,
                        createBtn,
                        cancelBtn
                )
        );

        result.getStyleClass().add("p-2");

        return result;
    }

    private void clearErrors() {
        firstNameError.setText("");
        lastNameError.setText("");
    }

    private boolean validate() {
        this.clearErrors();

        if(this.firstNameField.getText().isEmpty())
            firstNameError.setText("Prašome užpildyti šį lauką!");

        if(this.lastNameField.getText().isEmpty())
            lastNameError.setText("Prašome užpildyti šį lauką!");

        return firstNameError.getText().isEmpty() && lastNameError.getText().isEmpty();
    }

    private void save() {
        if(!this.validate())
            return;

        Student student = new Student(
                firstNameField.getText(),
                lastNameField.getText()
        );

        Main.getInstance().getStudentManager().addStudent(student);
        Main.getInstance().stopTask();
    }

    private void cancel() {
        Main.getInstance().stopTask();
    }
}
