package kk.studentai.view;

import javafx.scene.Parent;
import javafx.scene.layout.HBox;
import kk.studentai.Main;
import kk.studentai.models.Student;
import kk.studentai.models.StudentGroup;
import kk.studentai.models.managers.StudentGroupListener;
import kk.studentai.models.managers.StudentListener;
import kk.studentai.view.components.*;

public class HomeView extends AbstractView implements StudentListener, StudentGroupListener {
    private StudentsTable studentsTable = new StudentsTable(Main.getInstance().getStudentManager().getStudents());
    private StudentGroupTable studentGroupTable = new StudentGroupTable(Main.getInstance().getStudentManager().getStudentGroups());
    private StudentGroupInspectionPanel studentGroupInspectionTable = new StudentGroupInspectionPanel();
    private StudentAttendancyPanel studentAttendancyPanel = new StudentAttendancyPanel();

    public HomeView()
    {
        super(1800, 720);

        this.studentsTable.addOptionsColumn();
        this.studentGroupTable.addOptionsColumn();

        Main.getInstance().getStudentManager().registerStudentListener(this);
        Main.getInstance().getStudentManager().registerStudentGroupListener(this);
    }

    @Override
    protected Parent init() {
        HBox result = new HBox(
                50,
                new Toolbar(),
                studentsTable,
                studentGroupTable,
                studentGroupInspectionTable,
                studentAttendancyPanel
        );

        result.getStyleClass().add("p-3");
        return result;
    }

    public void update() {
        this.studentsTable.refresh();
        this.studentGroupTable.refresh();
        this.studentGroupInspectionTable.update();
        this.studentAttendancyPanel.update();
    }

    @Override
    public void onStudentAdded(Student student) {
        studentsTable.getItems().add(student);
    }

    @Override
    public void onStudentRemoved(Student student) {
        studentsTable.getItems().remove(student);
    }

    @Override
    public void onStudentGroupAdded(StudentGroup group) {
        studentGroupTable.getItems().add(group);
    }

    @Override
    public void onStudentGroupRemoved(StudentGroup group) {
        studentGroupTable.getItems().remove(group);
    }
}
