package kk.studentai.view;

import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import kk.studentai.Main;
import kk.studentai.filters.StudentGroupFilter;
import kk.studentai.models.Student;
import kk.studentai.models.StudentGroup;
import kk.studentai.tasks.MarkAttendancyTask;
import kk.studentai.view.components.CHBox;
import kk.studentai.view.components.RedText;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ConfigureAttendancyGroupView extends AbstractView {
    private static final SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd");
    private ChoiceBox<StudentGroup> studentGroupChoice = new ChoiceBox<>();
    private TextField dateField = new TextField();
    private RedText studentGroupError = new RedText();
    private RedText dateError = new RedText();

    @Override
    protected Parent init() {
        this.studentGroupChoice.getItems().addAll(Main.getInstance().getStudentManager().getStudentGroups());

        Button createBtn = new Button("Tęsti");
        Button cancelBtn = new Button("Atšaukti");

        createBtn.setOnAction(evt -> this.save());
        cancelBtn.setOnAction(evt -> this.cancel());

        VBox result = new VBox(
                25,
                new VBox(
                        new Label("Grupė:"),
                        studentGroupChoice,
                        studentGroupError
                ),
                new VBox(
                        new Label("Data:"),
                        dateField,
                        dateError
                ),
                new CHBox(
                        25,
                        createBtn,
                        cancelBtn
                )
        );

        result.getStyleClass().add("p-2");

        return result;
    }

    private void clearErrors() {
        dateError.setText("");
        studentGroupError.setText("");
    }

    private boolean validate() {
        this.clearErrors();

        StudentGroup studentGroup = this.studentGroupChoice.getSelectionModel().getSelectedItem();

        if (studentGroup == null)
            studentGroupError.setText("Prašome pasirinkti reikšmę!");

        if (this.dateField.getText().isEmpty())
            dateError.setText("Prašome užpildyti šį lauką!");

        else {
            try {
                dateParser.parse(dateField.getText());
            } catch (ParseException exception) {
                dateError.setText("Nepavyko konvertuoti reikšmės į datą!");
            }
        }

        return dateError.getText().isEmpty() && studentGroupError.getText().isEmpty();
    }

    private void save() {
        if (!this.validate())
            return;

        StudentGroup studentGroup = this.studentGroupChoice.getSelectionModel().getSelectedItem();
        Date date;

        try {
            date = dateParser.parse(dateField.getText());
        } catch (ParseException exception) {
            dateError.setText("Nepavyko konvertuoti reikšmės į datą!");
            return;
        }

        Main.getInstance().launchTask(new MarkAttendancyTask(
                new StudentGroupFilter(studentGroup),
                date
        ));
    }

    private void cancel() {
        Main.getInstance().stopTask();
    }
}
