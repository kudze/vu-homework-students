package kk.studentai.view;

import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import kk.studentai.Main;
import kk.studentai.models.Student;
import kk.studentai.models.StudentGroup;
import kk.studentai.view.components.CHBox;
import kk.studentai.view.components.RedText;

public class CreateStudentGroupView extends AbstractView {
    private TextField titleField = new TextField();
    private RedText titleError = new RedText();

    @Override
    protected Parent init() {
        Button createBtn = new Button("Išsaugoti");
        Button cancelBtn = new Button("Atšaukti");

        createBtn.setOnAction(evt -> this.save());
        cancelBtn.setOnAction(evt -> this.cancel());

        VBox result = new VBox(
                25,
                new VBox(
                        new Label("Pavadinimas:"),
                        titleField,
                        titleError
                ),
                new CHBox(
                        25,
                        createBtn,
                        cancelBtn
                )
        );

        result.getStyleClass().add("p-2");

        return result;
    }

    private void clearErrors() {
        titleError.setText("");
    }

    private boolean validate() {
        this.clearErrors();

        if(this.titleField.getText().isEmpty())
            titleError.setText("Prašome užpildyti šį lauką!");

        return titleError.getText().isEmpty();
    }

    private void save() {
        if(!this.validate())
            return;

        StudentGroup group = new StudentGroup(titleField.getText());

        Main.getInstance().getStudentManager().addStudentGroup(group);
        Main.getInstance().stopTask();
    }

    private void cancel() {
        Main.getInstance().stopTask();
    }
}
