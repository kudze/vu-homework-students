package kk.studentai.view;

import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import kk.studentai.Main;
import kk.studentai.filters.StudentFilter;
import kk.studentai.models.Student;
import kk.studentai.models.StudentGroup;
import kk.studentai.tasks.MarkAttendancyTask;
import kk.studentai.view.components.CHBox;
import kk.studentai.view.components.RedText;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ConfigureAttendancyStudentView extends AbstractView {
    private static final SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd");
    private ChoiceBox<Student> studentChoice = new ChoiceBox<>();
    private TextField dateField = new TextField();
    private RedText studentError = new RedText();
    private RedText dateError = new RedText();

    @Override
    protected Parent init() {
        this.studentChoice.getItems().addAll(Main.getInstance().getStudentManager().getStudents());

        Button createBtn = new Button("Tęsti");
        Button cancelBtn = new Button("Atšaukti");

        createBtn.setOnAction(evt -> this.save());
        cancelBtn.setOnAction(evt -> this.cancel());

        VBox result = new VBox(
                25,
                new VBox(
                        new Label("Studentas:"),
                        studentChoice,
                        studentError
                ),
                new VBox(
                        new Label("Data:"),
                        dateField,
                        dateError
                ),
                new CHBox(
                        25,
                        createBtn,
                        cancelBtn
                )
        );

        result.getStyleClass().add("p-2");

        return result;
    }

    private void clearErrors() {
        studentError.setText("");
        dateError.setText("");
    }

    private boolean validate() {
        this.clearErrors();

        Student student = this.studentChoice.getSelectionModel().getSelectedItem();

        if (student == null)
            studentError.setText("Prašome pasirinkti reikšmę!");

        if (this.dateField.getText().isEmpty())
            dateError.setText("Prašome užpildyti šį lauką!");

        else {
            try {
                dateParser.parse(dateField.getText());
            } catch (ParseException exception) {
                dateError.setText("Nepavyko konvertuoti reikšmės į datą!");
            }
        }

        return studentError.getText().isEmpty() && dateError.getText().isEmpty();
    }

    private void save() {
        if (!this.validate())
            return;

        Student student = this.studentChoice.getSelectionModel().getSelectedItem();
        Date date;

        try {
            date = dateParser.parse(dateField.getText());
        } catch (ParseException exception) {
            dateError.setText("Nepavyko konvertuoti reikšmės į datą!");
            return;
        }

        Main.getInstance().launchTask(new MarkAttendancyTask(
                new StudentFilter(student),
                date
        ));
    }

    private void cancel() {
        Main.getInstance().stopTask();
    }
}
