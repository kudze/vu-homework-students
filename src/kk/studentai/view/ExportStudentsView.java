package kk.studentai.view;

import com.opencsv.CSVWriter;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import kk.studentai.Main;
import kk.studentai.models.Student;
import kk.studentai.models.StudentGroup;
import kk.studentai.view.components.CHBox;
import kk.studentai.view.components.RedText;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

public class ExportStudentsView extends AbstractView {
    private static final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
    private final HashMap<Student, CheckBox> checkboxes = new HashMap<>();
    private final ChoiceBox<String> choiceBox = new ChoiceBox<>();
    private final RedText checkboxError = new RedText();
    private final RedText choiceBoxError = new RedText();

    public ExportStudentsView() {
        choiceBox.getItems().addAll("CSV", "XLSX");

        for (Student student : Main.getInstance().getStudentManager().getStudents()) {
            CheckBox checkbox = new CheckBox(student.toString());
            checkboxes.put(student, checkbox);
        }
    }

    @Override
    protected Parent init() {
        Button createBtn = new Button("Eksportuoti");
        Button cancelBtn = new Button("Atšaukti");

        createBtn.setOnAction(evt -> this.export());
        cancelBtn.setOnAction(evt -> this.cancel());

        VBox checkboxesVBOX = new VBox(
                5,
                new Label("Pasirinkite studentus:")
        );

        for (Map.Entry<Student, CheckBox> pair : checkboxes.entrySet()) {
            checkboxesVBOX.getChildren().add(pair.getValue());
        }
        checkboxesVBOX.getChildren().add(checkboxError);

        VBox result = new VBox(
                25,
                checkboxesVBOX,
                new VBox(
                        new Label("Formatas:"),
                        choiceBox,
                        choiceBoxError
                ),
                new CHBox(
                        25,
                        createBtn,
                        cancelBtn
                )
        );

        result.getStyleClass().add("p-2");

        return result;
    }

    private void clearErrors() {
        this.checkboxError.setText("");
        this.choiceBoxError.setText("");
    }

    private boolean isAtleastOneCheckBoxMarked() {
        for (Map.Entry<Student, CheckBox> pair : checkboxes.entrySet())
            if (pair.getValue().isSelected())
                return true;

        return false;
    }

    private boolean validate() {
        this.clearErrors();

        if (!this.isAtleastOneCheckBoxMarked())
            this.checkboxError.setText("Pažymėkite bent 1 studentą!");

        if (this.choiceBox.getSelectionModel().getSelectedItem() == null)
            this.choiceBoxError.setText("Pasirinkite formatą");

        return choiceBoxError.getText().isEmpty() && checkboxError.getText().isEmpty();
    }

    private List<Student> getSelectedStudents() {
        ArrayList<Student> result = new ArrayList<>();

        for (Map.Entry<Student, CheckBox> pair : checkboxes.entrySet())
            if (pair.getValue().isSelected())
                result.add(pair.getKey());

        return result;
    }

    private List<Date> getActiveDatesFromStudents(List<Student> students) {
        ArrayList<Date> dates = new ArrayList<>();

        for (Student student : students) {
            for (Map.Entry<Date, Boolean> pair : student.getAttendancy().entrySet()) {
                if (!dates.contains(pair.getKey()))
                    dates.add(pair.getKey());
            }
        }

        Collections.sort(dates);

        return dates;
    }

    private void exportCSV() {
        List<Student> students = this.getSelectedStudents();
        List<Date> dates = this.getActiveDatesFromStudents(students);

        try {
            Writer writer = Files.newBufferedWriter(Paths.get("./studentai.csv"));

            CSVWriter csvWriter = new CSVWriter(
                    writer,
                    ';',
                    CSVWriter.DEFAULT_QUOTE_CHARACTER,
                    CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                    CSVWriter.DEFAULT_LINE_END
            );

            ArrayList<String> heading = new ArrayList<>();
            heading.add("Vardas");
            heading.add("Pavarde");
            heading.add("Grupes");

            for(Date date : dates) {
                heading.add(dateFormatter.format(date));
            }

            csvWriter.writeNext(heading.toArray(new String[heading.size()]));

            for (Student student : students) {
                ArrayList<String> data = new ArrayList<>();
                data.add(student.getFirstName());
                data.add(student.getLastName());

                ArrayList<String> groupNames = new ArrayList<>();
                for(StudentGroup group : student.getGroups())
                    groupNames.add(group.getTitle());

                data.add(String.join("|", groupNames));

                for(Date date : dates)
                    data.add(student.getAttendancyForDate(date));
                
                csvWriter.writeNext(data.toArray(new String[data.size()]));
            }

            csvWriter.close();
            writer.close();
        } catch (IOException ignored) {
        }
    }

    private void exportXLSX() {
        List<Student> students = this.getSelectedStudents();
        List<Date> dates = this.getActiveDatesFromStudents(students);

        int currRow = 0;
        int currCol = 0;

        try {
            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet sheet = workbook.createSheet("Studentai");
            Row row = null;
            Cell cell = null;

            row = sheet.createRow(currRow++);

            cell = row.createCell(currCol++);
            cell.setCellValue("Vardas");

            cell = row.createCell(currCol++);
            cell.setCellValue("Pavardė");

            cell = row.createCell(currCol++);
            cell.setCellValue("Grupės");

            for(Date date : dates) {
                cell = row.createCell(currCol++);
                cell.setCellValue(dateFormatter.format(date));
            }

            for (Student student : students) {
                row = sheet.createRow(currRow++);
                currCol = 0;

                cell = row.createCell(currCol++);
                cell.setCellValue(student.getFirstName());

                cell = row.createCell(currCol++);
                cell.setCellValue(student.getLastName());

                ArrayList<String> groupNames = new ArrayList<>();
                for(StudentGroup group : student.getGroups())
                    groupNames.add(group.getTitle());

                cell = row.createCell(currCol++);
                cell.setCellValue(String.join("|", groupNames));

                for(Date date : dates) {
                    cell = row.createCell(currCol++);
                    cell.setCellValue(student.getAttendancyForDate(date));
                }
            }

            FileOutputStream outputStream = new FileOutputStream("studentai.xlsx");
            workbook.write(outputStream);
            outputStream.close();
        } catch (IOException ignored) {
        }
    }

    public void export() {
        if (!this.validate())
            return;

        if (choiceBox.getSelectionModel().getSelectedItem().equalsIgnoreCase("CSV"))
            this.exportCSV();

        else
            this.exportXLSX();

        Main.getInstance().stopTask();
    }

    public void cancel() {
        Main.getInstance().stopTask();
    }

}
