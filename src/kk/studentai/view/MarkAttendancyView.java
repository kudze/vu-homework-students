package kk.studentai.view;

import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.util.Pair;
import kk.studentai.Main;
import kk.studentai.filters.StudentFilter;
import kk.studentai.filters.StudentFilterInterface;
import kk.studentai.models.Student;
import kk.studentai.tasks.MarkAttendancyTask;
import kk.studentai.view.components.CHBox;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MarkAttendancyView extends AbstractView {
    private static final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
    private Date date;
    private HashMap<Student, CheckBox> checkboxes = new HashMap<>();

    public MarkAttendancyView(StudentFilterInterface filter, Date date) {
        this.date = date;

        for (Student student : filter.filterStudents(Main.getInstance().getStudentManager().getStudents())) {
            CheckBox checkBox = new CheckBox(student.toString());

            if (student.getAttendancy().containsKey(date))
                checkBox.setSelected(student.getAttendancy().get(date));

            checkboxes.put(student, checkBox);
        }
    }

    @Override
    protected Parent init() {
        Button createBtn = new Button("Tęsti");
        Button cancelBtn = new Button("Atšaukti");

        createBtn.setOnAction(evt -> this.save());
        cancelBtn.setOnAction(evt -> this.cancel());

        VBox checkboxesVBOX = new VBox(
                5,
                new Label(dateFormatter.format(date) + " lankomumas:")
        );
        checkboxesVBOX.getChildren().addAll(checkboxes.values());

        VBox result = new VBox(
                25,
                checkboxesVBOX,
                new CHBox(
                        25,
                        createBtn,
                        cancelBtn
                )
        );
        result.getStyleClass().add("p-2");

        return result;
    }

    private void save() {
        for (Map.Entry<Student, CheckBox> pair : checkboxes.entrySet()) {
            pair.getKey().getAttendancy().put(date, pair.getValue().isSelected());
        }

        Main.getInstance().stopTask();
    }

    private void cancel() {
        Main.getInstance().stopTask();
    }

    public Date getDate() {
        return date;
    }
}
