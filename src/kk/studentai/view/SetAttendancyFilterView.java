package kk.studentai.view;

import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import kk.studentai.Main;
import kk.studentai.filters.StudentFilter;
import kk.studentai.models.Student;
import kk.studentai.tasks.MarkAttendancyTask;
import kk.studentai.view.components.CHBox;
import kk.studentai.view.components.RedText;
import kk.studentai.view.components.StudentAttendancyFilter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SetAttendancyFilterView extends AbstractView {
    private static final SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd");
    private TextField dateStartField = new TextField();
    private TextField dateEndField = new TextField();
    private RedText dateStartError = new RedText();
    private RedText dateEndError = new RedText();
    private StudentAttendancyFilter filter;

    public SetAttendancyFilterView(StudentAttendancyFilter filter)
    {
        this.filter = filter;

        if(this.filter.getDateFrom() != null)
            this.dateStartField.setText(dateParser.format(this.filter.getDateFrom()));

        if(this.filter.getDateTo() != null)
            this.dateEndField.setText(dateParser.format(this.filter.getDateTo()));
    }

    @Override
    protected Parent init() {
        Button createBtn = new Button("Išsaugoti");
        Button cancelBtn = new Button("Atšaukti");
        Button clearBtn = new Button("Išvalyti");

        createBtn.setOnAction(evt -> this.save());
        cancelBtn.setOnAction(evt -> this.cancel());
        clearBtn.setOnAction(evt -> this.clear());

        VBox result = new VBox(
                25,
                new VBox(
                        new Label("Data nuo:"),
                        dateStartField,
                        dateStartError
                ),
                new VBox(
                        new Label("Data iki:"),
                        dateEndField,
                        dateEndError
                ),
                new CHBox(
                        25,
                        createBtn,
                        cancelBtn
                )
        );

        result.getStyleClass().add("p-2");

        return result;
    }

    private void clearErrors() {
        dateStartError.setText("");
        dateEndError.setText("");
    }

    private boolean validate() {
        this.clearErrors();

        Date dateFrom = null, dateTo = null;

        if (this.dateStartField.getText().isEmpty())
            dateStartError.setText("Prašome užpildyti šį lauką!");

        else {
            try {
                dateFrom = dateParser.parse(dateStartField.getText());
            } catch (ParseException exception) {
                dateStartError.setText("Nepavyko konvertuoti reikšmės į datą!");
            }
        }

        if (this.dateEndField.getText().isEmpty())
            dateEndError.setText("Prašome užpildyti šį lauką!");

        else {
            try {
                dateTo = dateParser.parse(dateEndField.getText());
            } catch (ParseException exception) {
                dateEndError.setText("Nepavyko konvertuoti reikšmės į datą!");
            }
        }

        if(dateFrom != null && dateTo != null && dateTo.before(dateFrom))
        {
            dateEndError.setText("Pabaigos data yra mažesnė už pradžios datą");
        }

        return dateStartError.getText().isEmpty() && dateEndError.getText().isEmpty();
    }

    private void save() {
        if (!this.validate())
            return;

        Date dateFrom;
        Date dateTo;

        try {
            dateFrom = dateParser.parse(this.dateStartField.getText());
            dateTo = dateParser.parse(this.dateEndField.getText());
        } catch (ParseException exception) {
            return;
        }

        this.filter.setDateFrom(dateFrom);
        this.filter.setDateTo(dateTo);

        Main.getInstance().stopTask();
    }

    private void cancel() {
        Main.getInstance().stopTask();
    }

    private void clear() {
        this.filter.setDateFrom(null);
        this.filter.setDateTo(null);

        Main.getInstance().stopTask();
    }
}
