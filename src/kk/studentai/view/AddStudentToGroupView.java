package kk.studentai.view;

import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import kk.studentai.Main;
import kk.studentai.models.Student;
import kk.studentai.models.StudentGroup;
import kk.studentai.view.components.CHBox;
import kk.studentai.view.components.RedText;

public class AddStudentToGroupView extends AbstractView {
    private ChoiceBox<Student> studentChoice = new ChoiceBox<>();
    private ChoiceBox<StudentGroup> studentGroupChoice = new ChoiceBox<>();
    private RedText studentError = new RedText();
    private RedText studentGroupError = new RedText();

    @Override
    protected Parent init() {
        this.studentChoice.getItems().addAll(Main.getInstance().getStudentManager().getStudents());
        this.studentGroupChoice.getItems().addAll(Main.getInstance().getStudentManager().getStudentGroups());

        Button createBtn = new Button("Išsaugoti");
        Button cancelBtn = new Button("Atšaukti");

        createBtn.setOnAction(evt -> this.save());
        cancelBtn.setOnAction(evt -> this.cancel());

        VBox result = new VBox(
                25,
                new VBox(
                        new Label("Studentas:"),
                        studentChoice,
                        studentError
                ),
                new VBox(
                        new Label("Grupė:"),
                        studentGroupChoice,
                        studentGroupError
                ),
                new CHBox(
                        25,
                        createBtn,
                        cancelBtn
                )
        );

        result.getStyleClass().add("p-2");

        return result;
    }

    private void clearErrors() {
        studentError.setText("");
        studentGroupError.setText("");
    }

    private boolean validate() {
        this.clearErrors();

        Student student = this.studentChoice.getSelectionModel().getSelectedItem();
        StudentGroup studentGroup = this.studentGroupChoice.getSelectionModel().getSelectedItem();

        if(student == null)
            studentError.setText("Prašome pasirinkti reikšmę!");

        if(studentGroup == null)
            studentGroupError.setText("Prašome pasirinkti reikšmę!");

        if(studentError.getText().isEmpty() && studentGroupError.getText().isEmpty() && studentGroup.getStudents().contains(student))
            studentGroupError.setText("Šis studentas jau yra šioje grupėje!");

        return studentError.getText().isEmpty() && studentGroupError.getText().isEmpty();
    }

    private void save() {
        if(!this.validate())
            return;

        Student student = this.studentChoice.getSelectionModel().getSelectedItem();
        StudentGroup studentGroup = this.studentGroupChoice.getSelectionModel().getSelectedItem();

        studentGroup.addStudent(student);

        Main.getInstance().stopTask();
    }

    private void cancel() {
        Main.getInstance().stopTask();
    }
}
