package kk.studentai.view;

import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;
import kk.studentai.Main;
import kk.studentai.tasks.ConfigureAttendancyGroupTask;
import kk.studentai.tasks.ConfigureAttendancyStudentTask;
import kk.studentai.view.components.CHBox;
import kk.studentai.view.components.RedText;

public class ConfigureAttendancyView extends AbstractView {
    private RadioButton radioFilterStudent = new RadioButton("Noriu naudoti studento filtrą");
    private RadioButton radioFilterGroup = new RadioButton("Noriu naudoti grupės filtrą");
    private ToggleGroup toggleGroup = new ToggleGroup();
    private RedText error = new RedText();

    @Override
    protected Parent init() {
        radioFilterStudent.setToggleGroup(toggleGroup);
        radioFilterGroup.setToggleGroup(toggleGroup);

        Button createBtn = new Button("Tęsti");
        Button cancelBtn = new Button("Atšaukti");

        createBtn.setOnAction(evt -> this.save());
        cancelBtn.setOnAction(evt -> this.cancel());

        VBox result = new VBox(
                25,
                new VBox(
                        5,
                        radioFilterStudent,
                        radioFilterGroup,
                        error
                ),
                new CHBox(
                        25,
                        createBtn,
                        cancelBtn
                )
        );

        result.getStyleClass().add("p-2");

        return result;
    }

    private void clearErrors() {
        error.setText("");
    }

    private boolean validate() {
        this.clearErrors();

        if(toggleGroup.getSelectedToggle() == null)
            error.setText("Pasirinkite vieną iš pasirinkimų!");

        return error.getText().isEmpty();
    }

    private void save() {
        if(!this.validate())
            return;

        if(toggleGroup.getSelectedToggle() == this.radioFilterStudent)
            Main.getInstance().launchTask(new ConfigureAttendancyStudentTask());

        else if(toggleGroup.getSelectedToggle() == this.radioFilterGroup)
            Main.getInstance().launchTask(new ConfigureAttendancyGroupTask());

        else
            error.setText("Įvyko nežinoma klaida");
    }

    private void cancel() {
        Main.getInstance().stopTask();
    }
}
