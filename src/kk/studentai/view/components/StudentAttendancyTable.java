package kk.studentai.view.components;

import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.text.Text;
import javafx.util.Callback;
import kk.studentai.models.Student;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class StudentAttendancyTable extends StudentsTable {
    private static final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
    private StudentAttendancyPanel panel;

    public StudentAttendancyTable(StudentAttendancyPanel panel)
    {
        super(null);

        this.panel = panel;

        this.initializeAttendancyCollumns();
    }

    public void update() {
        this.getColumns().clear();
        this.initializeColumns();
        this.initializeAttendancyCollumns();
        this.refresh();
    }

    public static Date incrementDayForDate(Date date)
    {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 1);
        return c.getTime();
    }

    private void initializeAttendancyCollumns()
    {
        StudentAttendancyFilter filter = this.panel.getFilter();
        Date dateFrom = filter.getDateFrom(), dateTo = filter.getDateTo();

        if(dateFrom == null || dateTo == null)
            return;

        for(Date i = new Date(dateFrom.getTime()); i.before(dateTo) || i.equals(dateTo); i = incrementDayForDate(i))
        {
            TableColumn<Student, Void> column = new TableColumn<>(dateFormatter.format(i));

            Date finalI = new Date(i.getTime());
            Callback<TableColumn<Student, Void>, TableCell<Student, Void>> cellFactory = new Callback<>() {
                @Override
                public TableCell<Student, Void> call(final TableColumn<Student, Void> param) {
                    return new TableCell<>() {
                        private final Text text = new Text();

                        @Override
                        public void updateItem(Void item, boolean empty) {
                            super.updateItem(item, empty);
                            if (empty) {
                                setGraphic(null);
                            } else {
                                Student student = getTableView().getItems().get(getIndex());
                                text.setText(student.getAttendancyForDate(finalI));
                                setGraphic(text);
                            }
                        }
                    };
                }
            };

            column.setCellFactory(cellFactory);

            this.getColumns().add(column);
        }
    }

    public StudentAttendancyPanel getPanel() {
        return panel;
    }
}
