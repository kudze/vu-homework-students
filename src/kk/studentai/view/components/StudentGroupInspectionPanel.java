package kk.studentai.view.components;

import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import kk.studentai.Main;
import kk.studentai.models.Student;
import kk.studentai.models.StudentGroup;
import kk.studentai.models.StudentGroupContentsListener;
import kk.studentai.models.managers.StudentGroupListener;

import java.util.Collection;

public class StudentGroupInspectionPanel extends VBox implements StudentGroupListener, StudentGroupContentsListener {
    private StudentGroupInspectionTable table;
    private ChoiceBox<StudentGroup> choice;
    private StudentGroup selectedGroup;

    public StudentGroupInspectionPanel() {
        this(Main.getInstance().getStudentManager().getStudentGroups());
    }

    public StudentGroupInspectionPanel(Collection<StudentGroup> students) {
        super(20);

        this.choice = new ChoiceBox<StudentGroup>();
        this.table = new StudentGroupInspectionTable(this);
        this.table.addOptionsColumn();
        this.choice.getItems().addAll(Main.getInstance().getStudentManager().getStudentGroups());
        this.choice.setOnAction(evt -> this.updateChoice());

        Main.getInstance().getStudentManager().registerStudentGroupListener(this);

        this.getChildren().addAll(new HBox(20, new Label("Pasirinkite grupę:"), choice), table);
    }

    public void update()
    {
        this.getTable().refresh();

        StudentGroup selected = this.getChoice().getValue();
        this.getChoice().getItems().clear();
        this.getChoice().getItems().addAll(Main.getInstance().getStudentManager().getStudentGroups());

        if(this.getChoice().getItems().contains(selected))
            this.getChoice().setValue(selected);
        else
            this.getChoice().setValue(null);

        this.updateChoice();
    }

    public void updateChoice()
    {
        this.table.getItems().clear();

        if(selectedGroup != null) {
            selectedGroup.unregisterListener(this);
            selectedGroup = null;
        }

        StudentGroup group = choice.getSelectionModel().getSelectedItem();
        if(group == null)
            return;

        group.registerListener(this);
        this.table.getItems().addAll(group.getStudents());

        selectedGroup = group;
    }

    @Override
    public void onStudentGroupAdded(StudentGroup group) {
        this.choice.getItems().add(group);
    }

    @Override
    public void onStudentGroupRemoved(StudentGroup group) {
        this.choice.getItems().remove(group);

        if(group == selectedGroup) {
            this.choice.setValue(null);
            this.updateChoice();
        }
    }

    @Override
    public void onStudentAddedToGroup(Student student) {
        this.table.getItems().add(student);
    }

    @Override
    public void onStudentRemovedFromGroup(Student student) {
        this.table.getItems().remove(student);
    }

    public StudentGroupInspectionTable getTable() {
        return table;
    }

    public ChoiceBox<StudentGroup> getChoice() {
        return choice;
    }
}
