package kk.studentai.view.components;

import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.util.Callback;
import kk.studentai.models.Student;

public class StudentGroupInspectionTable extends StudentsTable {
    private StudentGroupInspectionPanel panel;

    public StudentGroupInspectionTable(StudentGroupInspectionPanel panel) {
        super(null);

        this.panel = panel;
    }

    @Override
    public void addOptionsColumn() {
        TableColumn<Student, Void> colBtn = new TableColumn("Parinktys");
        colBtn.setPrefWidth(120);

        Callback<TableColumn<Student, Void>, TableCell<Student, Void>> cellFactory = new Callback<>() {
            @Override
            public TableCell<Student, Void> call(final TableColumn<Student, Void> param) {
                return new TableCell<Student, Void>() {
                    private final Button removeBtn = new Button("Išimti iš grupės");

                    {
                        removeBtn.setOnAction(evt -> {
                            Student student = getTableView().getItems().get(getIndex());
                            panel.getChoice().getSelectionModel().getSelectedItem().removeStudent(student);
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(removeBtn);
                        }
                    }
                };
            }
        };

        colBtn.setCellFactory(cellFactory);

        this.getColumns().add(colBtn);
    }
}
