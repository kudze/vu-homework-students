package kk.studentai.view.components;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.HBox;

public class CHBox extends HBox {

    public CHBox() {
        this.setAlignment(Pos.CENTER);
    }

    public CHBox(double v) {
        super(v);
        this.setAlignment(Pos.CENTER);
    }

    public CHBox(Node... nodes) {
        super(nodes);
        this.setAlignment(Pos.CENTER);
    }

    public CHBox(double v, Node... nodes) {
        super(v, nodes);
        this.setAlignment(Pos.CENTER);
    }
}
