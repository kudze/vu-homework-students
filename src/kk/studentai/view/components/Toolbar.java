package kk.studentai.view.components;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import kk.studentai.Main;
import kk.studentai.tasks.*;

public class Toolbar extends VBox {

    public Toolbar()
    {
        super(20);
        this.setAlignment(Pos.CENTER);

        Button addStudentBtn = new Button("Pridėti studentą");
        addStudentBtn.setOnAction(evt -> this.startStudentAdd());

        Button addStudentGroupBtn = new Button("Pridėti studentų grupę");
        addStudentGroupBtn.setOnAction(evt -> this.startStudentGroupAdd());

        Button addStudentToGroupBtn = new Button("Pridėti studentą prie grupės");
        addStudentToGroupBtn.setOnAction(evt -> this.startStudentAddToGroup());

        Button markAttendancyBtn = new Button("Žymėti lankomumą");
        markAttendancyBtn.setOnAction(evt -> this.startMarkAttendancy());

        Button exportStudentsBtn = new Button("Eksportuoti studentus");
        exportStudentsBtn.setOnAction(evt -> this.startExportStudents());

        Button importStudentsBtn = new Button("Importuoti studentus");
        importStudentsBtn.setOnAction(evt -> this.startImportStudents());

        this.getChildren().addAll(addStudentBtn, addStudentGroupBtn, addStudentToGroupBtn, markAttendancyBtn, exportStudentsBtn, importStudentsBtn);
    }

    private void startStudentAdd()
    {
        Main.getInstance().launchTask(new CreateStudentTask());
    }

    private void startStudentGroupAdd() {
        Main.getInstance().launchTask(new CreateStudentGroupTask());
    }

    private void startStudentAddToGroup() {
        Main.getInstance().launchTask(new AddStudentToGroupTask());
    }

    private void startExportStudents() {
        Main.getInstance().launchTask(new ExportStudentsTask());
    }

    private void startImportStudents() {
        Main.getInstance().launchTask(new ImportStudentsTask());
    }

    private void startMarkAttendancy() {
        Main.getInstance().launchTask(new ConfigureAttendancyTask());
    }
}
