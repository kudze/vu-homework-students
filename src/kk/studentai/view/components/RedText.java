package kk.studentai.view.components;

import javafx.scene.paint.Color;
import javafx.scene.text.Text;

public class RedText extends Text {
    public RedText() {
        super();
        this.setFill(Color.RED);
    }

    public RedText(String s) {
        super(s);
        this.setFill(Color.RED);
    }

    public RedText(double v, double v1, String s) {
        super(v, v1, s);
        this.setFill(Color.RED);
    }
}
