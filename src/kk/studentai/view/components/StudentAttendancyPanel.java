package kk.studentai.view.components;

import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import kk.studentai.Main;
import kk.studentai.models.Student;
import kk.studentai.models.StudentGroup;
import kk.studentai.models.StudentGroupContentsListener;
import kk.studentai.models.managers.StudentGroupListener;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class StudentAttendancyPanel extends VBox implements StudentGroupListener, StudentGroupContentsListener {
    private static final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
    private final StudentAttendancyTable table;
    private final StudentAttendancyFilter filter;
    private final ChoiceBox<StudentGroup> choice;
    private StudentGroup selectedGroup;

    public StudentAttendancyPanel() {
        this(Main.getInstance().getStudentManager().getStudentGroups());
    }

    public StudentAttendancyPanel(Collection<StudentGroup> students) {
        super(20);

        this.choice = new ChoiceBox<StudentGroup>();
        this.filter = new StudentAttendancyFilter(this);
        this.table = new StudentAttendancyTable(this);
        this.choice.getItems().addAll(Main.getInstance().getStudentManager().getStudentGroups());
        this.choice.setOnAction(evt -> this.updateChoice());

        Button saveToPDFBtn = new Button("Išsaugoti į PDF");
        saveToPDFBtn.setOnAction(evt -> this.saveToPDF());

        Main.getInstance().getStudentManager().registerStudentGroupListener(this);

        this.getChildren().addAll(
                new HBox(
                        20,
                        new Label("Pasirinkite grupę:"),
                        choice
                ),
                filter,
                table,
                new CHBox(saveToPDFBtn)
        );
    }

    public void update() {
        this.getTable().update();

        StudentGroup selected = this.getChoice().getValue();
        this.getChoice().getItems().clear();
        this.getChoice().getItems().addAll(Main.getInstance().getStudentManager().getStudentGroups());

        if (this.getChoice().getItems().contains(selected))
            this.getChoice().setValue(selected);
        else
            this.getChoice().setValue(null);

        this.updateChoice();
    }

    public void updateChoice() {
        this.table.getItems().clear();

        if (selectedGroup != null) {
            selectedGroup.unregisterListener(this);
            selectedGroup = null;
        }

        StudentGroup group = choice.getSelectionModel().getSelectedItem();
        if (group == null)
            return;

        group.registerListener(this);
        this.table.getItems().addAll(group.getStudents());

        selectedGroup = group;
    }

    private void drawTable(PDPage page, PDPageContentStream contentStream,
                           float y, float margin,
                           ArrayList<ArrayList<String>> content) throws IOException {
        final int rows = content.size();
        final int cols = content.get(0).size();
        final float rowHeight = 20f;
        final float tableWidth = page.getMediaBox().getWidth() - (2 * margin);
        final float tableHeight = rowHeight * rows;
        final float colWidth = tableWidth / (float) cols;
        final float cellMargin = 5f;

        //draw the rows
        float nexty = y;
        for (int i = 0; i <= rows; i++) {
            contentStream.drawLine(margin, nexty, margin + tableWidth, nexty);
            nexty -= rowHeight;
        }

        //draw the columns
        float nextx = margin;
        for (int i = 0; i <= cols; i++) {
            contentStream.drawLine(nextx, y, nextx, y - tableHeight);
            nextx += colWidth;
        }

        //now add the text
        contentStream.setFont(PDType1Font.HELVETICA_BOLD, 12);

        float textx = margin + cellMargin;
        float texty = y - 15;
        for (int i = 0; i < content.size(); i++) {
            for (int j = 0; j < content.get(i).size(); j++) {
                String text = content.get(i).get(j);
                contentStream.beginText();
                contentStream.moveTextPositionByAmount(textx, texty);
                text = text.replaceAll("ą", "a");
                text = text.replaceAll("č", "c");
                text = text.replaceAll("ę", "e");
                text = text.replaceAll("ė", "e");
                text = text.replaceAll("į", "i");
                text = text.replaceAll("š", "s");
                text = text.replaceAll("ų", "u");
                text = text.replaceAll("ū", "u");
                text = text.replaceAll("ž", "z");
                text = text.replaceAll("Ą", "A");
                text = text.replaceAll("Č", "C");
                text = text.replaceAll("Ę", "E");
                text = text.replaceAll("Ė", "E");
                text = text.replaceAll("Į", "I");
                text = text.replaceAll("Š", "S");
                text = text.replaceAll("Ų", "U");
                text = text.replaceAll("Ū", "U");
                text = text.replaceAll("Ž", "Z");
                contentStream.drawString(text);
                contentStream.endText();
                textx += colWidth;
            }
            texty -= rowHeight;
            textx = margin + cellMargin;
        }
    }

    private void saveToPDF() {
        Date dateFrom = filter.getDateFrom(), dateTo = filter.getDateTo();

        if (dateFrom == null || dateTo == null)
            return;

        try {
            PDDocument document = new PDDocument();
            PDPage page = new PDPage();
            document.addPage(page);

            PDPageContentStream contentStream = new PDPageContentStream(document, page);

            ArrayList<ArrayList<String>> content = new ArrayList<>();
            ArrayList<String> heading = new ArrayList<>();
            heading.add("Vardas");
            heading.add("Pavarde");

            for (Date i = new Date(dateFrom.getTime()); i.before(dateTo) || i.equals(dateTo); i = StudentAttendancyTable.incrementDayForDate(i)) {
                heading.add(dateFormatter.format(i));
            }
            content.add(heading);

            StudentGroup group = this.getChoice().getSelectionModel().getSelectedItem();

            for (Student student : group.getStudents()) {
                ArrayList<String> row = new ArrayList<>();
                row.add(student.getFirstName());
                row.add(student.getLastName());

                for (Date i = new Date(dateFrom.getTime()); i.before(dateTo) || i.equals(dateTo); i = StudentAttendancyTable.incrementDayForDate(i)) {
                    row.add(student.getAttendancyForDate(i));
                }

                content.add(row);
            }

            drawTable(page, contentStream, 700, 100, content);

            contentStream.close();
            document.save("lankomumo_ataskaita.pdf");
        } catch (IOException ignored) {

        }
    }

    public StudentAttendancyTable getTable() {
        return table;
    }

    public ChoiceBox<StudentGroup> getChoice() {
        return choice;
    }

    public StudentAttendancyFilter getFilter() {
        return filter;
    }

    public StudentGroup getSelectedGroup() {
        return selectedGroup;
    }

    @Override
    public void onStudentAddedToGroup(Student student) {
        this.table.getItems().add(student);
    }

    @Override
    public void onStudentRemovedFromGroup(Student student) {
        this.table.getItems().remove(student);
    }

    @Override
    public void onStudentGroupAdded(StudentGroup group) {
        this.choice.getItems().add(group);
    }

    @Override
    public void onStudentGroupRemoved(StudentGroup group) {
        this.choice.getItems().remove(group);

        if (group == selectedGroup) {
            this.choice.setValue(null);
            this.updateChoice();
        }
    }
}
