package kk.studentai.view.components;

import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import kk.studentai.Main;
import kk.studentai.tasks.SetAttendancyFilterTask;

import java.text.SimpleDateFormat;
import java.util.Date;

public class StudentAttendancyFilter extends HBox {
    private static final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
    private final StudentAttendancyPanel panel;
    private final Text filterText = new Text();
    private Date dateFrom;
    private Date dateTo;

    public StudentAttendancyFilter(StudentAttendancyPanel panel)
    {
        super(20);

        this.panel = panel;

        this.updateFilterText();

        Button setFilterBtn = new Button("Nustatyti filtrą");
        setFilterBtn.setOnAction(evt -> this.startFilterSetTask());

        this.getChildren().addAll(filterText, setFilterBtn);
    }

    private void startFilterSetTask()
    {
        Main.getInstance().launchTask(new SetAttendancyFilterTask(this));
    }

    private void updateFilterText()
    {
        if(this.dateFrom == null || this.dateTo == null)
        {
            this.filterText.setText("Filtras: nėra");
            return;
        }

        this.filterText.setText("Filtras: nuo " + dateFormatter.format(dateFrom) + " iki " + dateFormatter.format(dateTo));
    }

    public StudentAttendancyPanel getPanel() {
        return panel;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;

        this.updateFilterText();
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;

        this.updateFilterText();
    }
}
