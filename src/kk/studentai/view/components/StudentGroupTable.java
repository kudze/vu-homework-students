package kk.studentai.view.components;

import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import kk.studentai.Main;
import kk.studentai.models.Student;
import kk.studentai.models.StudentGroup;
import kk.studentai.tasks.EditStudentGroupTask;
import kk.studentai.tasks.EditStudentTask;

import java.util.Collection;

public class StudentGroupTable extends TableView<StudentGroup> {

    public StudentGroupTable() {
        this(Main.getInstance().getStudentManager().getStudentGroups());
    }

    public StudentGroupTable(Collection<StudentGroup> students) {
        super();

        TableColumn<StudentGroup, String> column1 = new TableColumn<>("Pavadinimas");
        column1.setCellValueFactory(new PropertyValueFactory<>("title"));

        TableColumn<StudentGroup, String> column2 = new TableColumn<>("Studentų skaičius");
        column2.setCellValueFactory(new PropertyValueFactory<>("studentCount"));

        this.getColumns().addAll(column1, column2);
        this.getItems().addAll(students);

        this.setPrefWidth(400);
    }

    public void addOptionsColumn()
    {
        TableColumn<StudentGroup, Void> colBtn = new TableColumn("Parinktys");

        Callback<TableColumn<StudentGroup, Void>, TableCell<StudentGroup, Void>> cellFactory = new Callback<TableColumn<StudentGroup, Void>, TableCell<StudentGroup, Void>>() {
            @Override
            public TableCell<StudentGroup, Void> call(final TableColumn<StudentGroup, Void> param) {
                return new TableCell<StudentGroup, Void>() {
                    private final Button editBtn = new Button("Redaguoti");
                    private final Button removeBtn = new Button("Ištrinti");
                    {
                        editBtn.setOnAction(evt -> {
                            StudentGroup group = getTableView().getItems().get(getIndex());
                            Main.getInstance().launchTask(new EditStudentGroupTask(group));
                        });

                        removeBtn.setOnAction(evt -> {
                            StudentGroup group = getTableView().getItems().get(getIndex());
                            Main.getInstance().getStudentManager().removeStudentGroup(group);
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(new HBox(5, editBtn, removeBtn));
                        }
                    }
                };
            }
        };

        colBtn.setCellFactory(cellFactory);

        this.getColumns().add(colBtn);
    }

}
