package kk.studentai.view.components;

import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import kk.studentai.Main;
import kk.studentai.models.Student;
import kk.studentai.tasks.EditStudentTask;

import java.util.Collection;

public class StudentsTable extends TableView<Student> {

    public StudentsTable() {
        this(Main.getInstance().getStudentManager().getStudents());
    }

    public StudentsTable(Collection<Student> students) {
        super();

        this.initializeColumns();

        if(students != null)
            this.getItems().addAll(students);

        this.setPrefWidth(300);
    }

    protected void initializeColumns()
    {
        TableColumn<Student, String> column1 = new TableColumn<>("Vardas");
        column1.setCellValueFactory(new PropertyValueFactory<>("firstName"));

        TableColumn<Student, String> column2 = new TableColumn<>("Pavardė");
        column2.setCellValueFactory(new PropertyValueFactory<>("lastName"));

        this.getColumns().addAll(column1, column2);
    }

    public void addOptionsColumn()
    {
        TableColumn<Student, Void> colBtn = new TableColumn("Parinktys");

        Callback<TableColumn<Student, Void>, TableCell<Student, Void>> cellFactory = new Callback<>() {
            @Override
            public TableCell<Student, Void> call(final TableColumn<Student, Void> param) {
                return new TableCell<>() {
                    private final Button editBtn = new Button("Redaguoti");
                    private final Button removeBtn = new Button("Ištrinti");

                    {
                        editBtn.setOnAction(evt -> {
                            Student student = getTableView().getItems().get(getIndex());
                            Main.getInstance().launchTask(new EditStudentTask(student));
                        });

                        removeBtn.setOnAction(evt -> {
                            Student student = getTableView().getItems().get(getIndex());
                            Main.getInstance().getStudentManager().removeStudent(student);
                        });
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(new HBox(5, editBtn, removeBtn));
                        }
                    }
                };
            }
        };

        colBtn.setCellFactory(cellFactory);

        this.getColumns().add(colBtn);
    }

}
