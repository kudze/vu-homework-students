package kk.studentai.view;

import javafx.scene.Parent;
import javafx.scene.Scene;

public abstract class AbstractView {
    private final int width;
    private final int height;

    public AbstractView() { this(0, 0); }

    public AbstractView(int width, int height)
    {
        this.width = width;
        this.height = height;
    }

    protected abstract Parent init();

    public Scene buildScene() {
        Scene scene;

        if(this.width == 0 && this.height == 0)
            scene = new Scene(this.init());

        else
            scene = new Scene(this.init(), this.width, this.height);

        scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());

        return scene;
    }
}
