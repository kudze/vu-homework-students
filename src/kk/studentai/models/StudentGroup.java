package kk.studentai.models;

import kk.studentai.Main;
import kk.studentai.models.managers.StudentListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StudentGroup implements StudentListener {
    private String title;
    private ArrayList<Student> students;
    private ArrayList<StudentGroupContentsListener> listeners = new ArrayList<>();

    public StudentGroup(String title)
    {
        this(title, new ArrayList<>());
    }

    public StudentGroup(String title, ArrayList<Student> students)
    {
        this.title = title;
        this.students = students;

        Main.getInstance().getStudentManager().registerStudentListener(this);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void addStudent(Student student)
    {
        this.students.add(student);

        for(StudentGroupContentsListener listener : listeners)
            listener.onStudentAddedToGroup(student);
    }

    public void removeStudent(Student student)
    {
        this.students.remove(student);

        for(StudentGroupContentsListener listener : listeners)
            listener.onStudentRemovedFromGroup(student);
    }

    public void registerListener(StudentGroupContentsListener listener)
    {
        this.listeners.add(listener);
    }

    public void unregisterListener(StudentGroupContentsListener listener)
    {
        this.listeners.remove(listener);
    }

    public List<Student> getStudents() {
        return Collections.unmodifiableList(students);
    }

    public int getStudentCount() {
        return this.students.size();
    }

    public void setStudents(ArrayList<Student> students) {
        this.students = students;
    }

    public ArrayList<StudentGroupContentsListener> getListeners() {
        return listeners;
    }

    @Override
    public void onStudentAdded(Student student) {

    }

    @Override
    public void onStudentRemoved(Student student) {
        this.removeStudent(student);
    }

    @Override
    public String toString() {
        return this.title;
    }
}
