package kk.studentai.models.managers;

import kk.studentai.models.Student;
import kk.studentai.models.StudentGroup;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class StudentManager {
    private static final String SAVE_FILENAME = "./duomenys.json";
    private static final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");

    private ArrayList<Student> students = new ArrayList<>();
    private ArrayList<StudentGroup> studentGroups = new ArrayList<>();
    private ArrayList<StudentListener> studentListener = new ArrayList<>();
    private ArrayList<StudentGroupListener> studentGroupListeners = new ArrayList<>();

    public void load() {
        try {
            String contents = Files.readString(Path.of(SAVE_FILENAME));
            JSONObject json = new JSONObject(contents);
            JSONArray studentsArray = json.getJSONArray("students");
            for (int i = 0; i < studentsArray.length(); i++) {
                JSONObject student = studentsArray.getJSONObject(i);
                JSONObject attendanceJSON = student.getJSONObject("attendance");
                TreeMap<Date, Boolean> attendance = new TreeMap<>();

                for (String dateSTR : attendanceJSON.keySet()) {
                    attendance.put(dateFormatter.parse(dateSTR), attendanceJSON.getBoolean(dateSTR));
                }

                students.add(new Student(
                        student.getString("first_name"),
                        student.getString("last_name"),
                        attendance
                ));
            }

            JSONArray studentGroupsArray = json.getJSONArray("students_groups");
            for (int i = 0; i < studentGroupsArray.length(); i++) {
                JSONObject group = studentGroupsArray.getJSONObject(i);
                JSONArray studentsJSON = group.getJSONArray("students");
                ArrayList<Student> students = new ArrayList<>();

                for (int j = 0; j < studentsJSON.length(); j++) {
                    students.add(this.students.get(studentsJSON.getInt(j)));
                }

                studentGroups.add(new StudentGroup(
                        group.getString("title"),
                        students
                ));
            }
        } catch (IOException | JSONException | ParseException exception) {
            System.out.println("Failed to read savefile!");
        }
    }

    public void save() {
        try {

            JSONArray studentsArray = new JSONArray();
            for (Student student : this.students) {
                JSONObject attendance = new JSONObject();
                for (Map.Entry<Date, Boolean> pair : student.getAttendancy().entrySet()) {
                    attendance.put(dateFormatter.format(pair.getKey()), pair.getValue());
                }

                JSONObject studentJSON = new JSONObject();
                studentJSON.put("first_name", student.getFirstName());
                studentJSON.put("last_name", student.getLastName());
                studentJSON.put("attendance", attendance);
                studentsArray.put(studentJSON);
            }

            JSONArray studentsGroupArray = new JSONArray();
            for(StudentGroup group : studentGroups)
            {
                JSONArray _studentsArray = new JSONArray();

                for(Student student : group.getStudents())
                {
                    _studentsArray.put(this.students.indexOf(student));
                }

                JSONObject groupJSON = new JSONObject();
                groupJSON.put("title", group.getTitle());
                groupJSON.put("students", _studentsArray);
                studentsGroupArray.put(groupJSON);
            }

            JSONObject json = new JSONObject();
            json.put("students", studentsArray);
            json.put("students_groups", studentsGroupArray);
            Files.writeString(Path.of(SAVE_FILENAME), json.toString());
        } catch (IOException exception) {
            System.out.println("Failed to write savefile!");
        }
    }

    public void addStudent(Student student) {
        this.students.add(student);

        for (StudentListener listener : this.studentListener)
            listener.onStudentAdded(student);
    }

    public void removeStudent(Student student) {
        this.students.remove(student);

        for (StudentListener listener : this.studentListener)
            listener.onStudentRemoved(student);
    }

    public void addStudentGroup(StudentGroup group) {
        this.studentGroups.add(group);

        for (StudentGroupListener listener : this.studentGroupListeners)
            listener.onStudentGroupAdded(group);
    }

    public void removeStudentGroup(StudentGroup group) {
        this.studentGroups.remove(group);

        for (StudentGroupListener listener : this.studentGroupListeners)
            listener.onStudentGroupRemoved(group);
    }

    public void registerStudentListener(StudentListener listener) {
        this.studentListener.add(listener);
    }

    public void unregisterStudentListener(StudentListener listener) {
        this.studentListener.remove(listener);
    }

    public void registerStudentGroupListener(StudentGroupListener listener) {
        this.studentGroupListeners.add(listener);
    }

    public void unregisterStudentGroupListener(StudentGroupListener listener) {
        this.studentGroupListeners.remove(listener);
    }

    public List<Student> getStudents() {
        return Collections.unmodifiableList(students);
    }

    public List<StudentGroup> getStudentGroups() {
        return Collections.unmodifiableList(studentGroups);
    }

    public StudentGroup getStudentGroupByTitle(String title)
    {
        for(StudentGroup group : this.studentGroups)
            if(group.getTitle().equals(title))
                return group;

        return null;
    }

    public List<StudentListener> getStudentListener() {
        return Collections.unmodifiableList(studentListener);
    }

    public ArrayList<StudentGroupListener> getStudentGroupListeners() {
        return studentGroupListeners;
    }
}
