package kk.studentai.models.managers;

import kk.studentai.models.StudentGroup;

public interface StudentGroupListener {
    void onStudentGroupAdded(StudentGroup group);
    void onStudentGroupRemoved(StudentGroup group);
}
