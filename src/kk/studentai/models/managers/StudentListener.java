package kk.studentai.models.managers;

import kk.studentai.models.Student;

public interface StudentListener {
    void onStudentAdded(Student student);
    void onStudentRemoved(Student student);
}
