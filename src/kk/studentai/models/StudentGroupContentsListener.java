package kk.studentai.models;

public interface StudentGroupContentsListener {
    void onStudentAddedToGroup(Student student);
    void onStudentRemovedFromGroup(Student student);
}
