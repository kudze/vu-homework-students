package kk.studentai.models;

import kk.studentai.Main;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;

public class Student {
    private String firstName;
    private String lastName;
    private TreeMap<Date, Boolean> attendancy;

    public Student(String firstName, String lastName)
    {
        this(firstName, lastName, new TreeMap<>());
    }

    public Student(String firstName, String lastName, TreeMap<Date, Boolean> attendancy)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.attendancy = attendancy;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public TreeMap<Date, Boolean> getAttendancy() {
        return attendancy;
    }

    public String getAttendancyForDate(Date date)
    {
        if(!this.attendancy.containsKey(date))
            return "Nenurodyta";

        if(this.attendancy.get(date))
            return "Dalyvavo";

        return "Nedalyvavo";
    }

    public List<StudentGroup> getGroups() {
        ArrayList<StudentGroup> result = new ArrayList<>();

        for(StudentGroup group : Main.getInstance().getStudentManager().getStudentGroups())
        {
            if(group.getStudents().contains(this))
                result.add(group);
        }

        return result;
    }

    @Override
    public String toString() {
        return this.firstName + " " + this.lastName;
    }
}
